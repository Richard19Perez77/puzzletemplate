package tac.toki.puzzleimage;

import sound.MyMediaPlayer;
import sound.MySoundPool;
import state.CommonVariables;
import surface.PuzzleSurface;
import surface.PuzzleSurface.PuzzleThread;
import tac.toki.puzzleimage.R;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * 
 * A class that handles the android life cycle events such as starting and
 * resuming the application. I started adding code to handle the sound
 * interrupts.
 * 
 */
public class MainActivity extends Activity {
	// static variables are used across classes for uniform values
	private static final int MENU_EASY = 1;
	private static final int MENU_HARD = 2;
	private static final int MENU_VERY_HARD = 3;
	private static final int MUSIC_TOGGLE = 8;
	private static final int SET_TOGGLE = 9;
	private static final int WIN_TOGGLE = 10;
	private static final int BORDER_TOGGLE = 11;
	public static final String MY_PREFERENCES = "MyPuzzle";

	private static final String COLUMN_DIFFICULTY = "DIFFICULTY";
	private static final String COLUMN_IMAGENUMBER = "IMAGENUMBER";
	private static final String COLUMN_SLOTS = "SLOTS";
	private static final String COLUMN_SOUND = "SOUND";
	private static final String COLUMN_CHIME = "CHIME";
	private static final String COLUMN_MUSIC = "MUSIC";
	private static final String COLUMN_BORDER = "BORDER";
	private static final String COLUMN_POSITION = "POSITION";

	// variables used in life cycle and menu driven events
	private PuzzleSurface puzzleSurface;
	private MyMediaPlayer myMediaPlayer;
	private MySoundPool mySoundPool;

	CommonVariables cv = CommonVariables.getInstance();

	NoisyAudioStreamReceiver myNoisyAudioStreamReceiver = new NoisyAudioStreamReceiver();

	SharedPreferences sharedpreferences;
	AudioManager audioManager;

	// start of receiver inner class to handle headphones becoming unplugged
	private class NoisyAudioStreamReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent
					.getAction())) {
				// quiet the media player
				myMediaPlayer.quietSound();
			}
		}
	}

	private IntentFilter intentFilter = new IntentFilter(
			AudioManager.ACTION_AUDIO_BECOMING_NOISY);

	private void startPlayback() {
		registerReceiver(myNoisyAudioStreamReceiver, intentFilter);
	}

	private void stopPlayback() {
		unregisterReceiver(myNoisyAudioStreamReceiver);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.puzzle_layout);

		myNoisyAudioStreamReceiver = new NoisyAudioStreamReceiver();

		sharedpreferences = getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);

		// check for all to be loaded here
		int acc = 0;
		if (sharedpreferences.contains(COLUMN_DIFFICULTY)) {
			cv.difficulty = sharedpreferences.getInt(COLUMN_DIFFICULTY, 0);
			acc++;
		}

		if (sharedpreferences.contains(COLUMN_IMAGENUMBER)) {
			cv.currentPuzzleImagePosition = sharedpreferences.getInt(
					COLUMN_IMAGENUMBER, 0);
			acc++;
		}

		if (sharedpreferences.contains(COLUMN_SLOTS)) {
			cv.setSlots(sharedpreferences.getString(COLUMN_SLOTS,
					"1,2,3,4,5,6,7,8,0"));
			acc++;
		}

		if (sharedpreferences.contains(COLUMN_SOUND)) {
			cv.playTapSound = sharedpreferences.getBoolean(COLUMN_SOUND, true);
			acc++;
		}

		if (sharedpreferences.contains(COLUMN_CHIME)) {
			cv.playChimeSound = sharedpreferences
					.getBoolean(COLUMN_CHIME, true);
			acc++;
		}

		if (sharedpreferences.contains(COLUMN_MUSIC)) {
			cv.playMusic = sharedpreferences.getBoolean(COLUMN_MUSIC, true);
			acc++;
		}

		if (sharedpreferences.contains(COLUMN_BORDER)) {
			cv.drawBorders = sharedpreferences.getBoolean(COLUMN_BORDER, true);
			acc++;
		}

		if (sharedpreferences.contains(COLUMN_POSITION)) {
			cv.currentSoundPosition = sharedpreferences.getInt(COLUMN_POSITION,
					0);
			acc++;
		}

		if (acc == 8) {
			cv.resumePreviousPuzzle = true;
		}

		AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

		cv.volume = (float) audioManager
				.getStreamVolume(AudioManager.STREAM_MUSIC)
				/ (float) audioManager
						.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

		setVolumeControlStream(AudioManager.STREAM_MUSIC);

		// reference the UI components
		referenceUIComponents();

		// music and sound setup
		audioInit();

		// puzzle should start paused
		puzzleSurface.puzzleThread.setState(PuzzleThread.STATE_PAUSE);
		
		Animation anim = AnimationUtils.loadAnimation(this, R.anim.intro);
		anim.reset();
		anim.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// this animation is longest and touch should be ready when this
				// completes
				puzzleSurface.introFinished = true;
			}
		});

		Animation anim2 = AnimationUtils.loadAnimation(this, R.anim.warp_out);
		anim2.reset();

		puzzleSurface = (PuzzleSurface) findViewById(R.id.puzzle);
		puzzleSurface.clearAnimation();
		puzzleSurface.startAnimation(anim);

		puzzleSurface.messageTextView = (TextView) findViewById(R.id.text);
		puzzleSurface.messageTextView.clearAnimation();
		puzzleSurface.messageTextView.startAnimation(anim2);
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onStart() {
		super.onStart();
		startPlayback();
	}

	@Override
	protected void onResume() {
		super.onResume();
		// register headphone listener
		if (myMediaPlayer != null)
			myMediaPlayer.resume();

		if (puzzleSurface != null) {
			puzzleSurface.onResume();
		}
	}

	private void audioInit() {
		// create new my media player
		myMediaPlayer = new MyMediaPlayer();
		myMediaPlayer.init();
		puzzleSurface.myMediaPlayer = myMediaPlayer;

		mySoundPool = new MySoundPool(15, AudioManager.STREAM_MUSIC, 100);
		cv.mySoundPool = mySoundPool;
	}

	private void referenceUIComponents() {
		// The UI has a puzzle
		puzzleSurface = (PuzzleSurface) findViewById(R.id.puzzle);

		cv.mStatusText = ((TextView) findViewById(R.id.text));
		cv.mSaveButton = ((Button) findViewById(R.id.nextButton));
		cv.rightWebLinkButton = ((ImageButton) findViewById(R.id.devartlinkbutton));
		cv.leftWebLinkButton = ((ImageButton) findViewById(R.id.facebooklinkbutton));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(0, MENU_EASY, 0, R.string.menu_easy);
		menu.add(0, MENU_HARD, 0, R.string.menu_hard);
		menu.add(0, MENU_VERY_HARD, 0, R.string.menu_very_hard);
		menu.add(0, SET_TOGGLE, 0, R.string.set_toggle);
		menu.add(0, WIN_TOGGLE, 0, R.string.win_toggle);
		menu.add(0, MUSIC_TOGGLE, 0, R.string.music_toggle);
		menu.add(0, BORDER_TOGGLE, 0, R.string.border_toggle);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_EASY:
			puzzleSurface.puzzleThread.setDifficulty(PuzzleSurface.EASY);
			return true;
		case MENU_HARD:
			puzzleSurface.puzzleThread.setDifficulty(PuzzleSurface.HARD);
			return true;
		case MENU_VERY_HARD:
			puzzleSurface.puzzleThread.setDifficulty(PuzzleSurface.VERY_HARD);
			return true;
		case MUSIC_TOGGLE:
			puzzleSurface.puzzleThread.toggleMusic();
			return true;
		case SET_TOGGLE:
			puzzleSurface.puzzleThread.toggleSetSound();
			return true;
		case WIN_TOGGLE:
			puzzleSurface.puzzleThread.toggleWinSound();
			return true;
		case BORDER_TOGGLE:
			puzzleSurface.puzzleThread.toggleBorder();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void nextImage(View v) {
		puzzleSurface.nextImage();
	}

	public void solve() {
		puzzleSurface.solve();
	}

	public void devArtActivity(View v) {
		puzzleSurface.devArtActivity();
	}

	public void facebookActivity(View v) {
		puzzleSurface.facebookActivity();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (myMediaPlayer != null)
			myMediaPlayer.pause();

		if (puzzleSurface != null)
			puzzleSurface.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
		// unregister sound handler
		stopPlayback();

		Editor editor = sharedpreferences.edit();
		editor.putInt(COLUMN_DIFFICULTY, cv.difficulty);
		editor.putInt(COLUMN_IMAGENUMBER, cv.currentPuzzleImagePosition);
		editor.putString(COLUMN_SLOTS, puzzleSurface.getSlotString());
		editor.putBoolean(COLUMN_SOUND, cv.playTapSound);
		editor.putBoolean(COLUMN_MUSIC, cv.playMusic);
		editor.putBoolean(COLUMN_CHIME, cv.playChimeSound);
		editor.putBoolean(COLUMN_BORDER, cv.drawBorders);
		if (myMediaPlayer != null && myMediaPlayer.mediaPlayer != null) {
			if (myMediaPlayer.mediaPlayer.isPlaying())
				myMediaPlayer.pause();
			cv.currentSoundPosition = myMediaPlayer.mediaPlayer
					.getCurrentPosition();
		}
		editor.putInt(COLUMN_POSITION, cv.currentSoundPosition);
		editor.commit();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mySoundPool != null) {
			mySoundPool.release();
			mySoundPool = null;
		}

		if (myMediaPlayer != null) {
			myMediaPlayer.cleanUp();
			myMediaPlayer = null;
		}

		if (puzzleSurface != null) {
			puzzleSurface.cleanUp();
			puzzleSurface = null;
		}

		cv = null;
		System.gc();
	}
}