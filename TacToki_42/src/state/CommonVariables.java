package state;

import java.util.ArrayList;
import java.util.Random;

import sound.MySoundPool;
import surface.PuzzlePiece;
import surface.PuzzleSlot;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * A class to hold variables that will be used across classes and the more
 * commonly used Context object.
 * 
 * Should be separated into common variables and common work.
 * 
 * @author Rick
 * 
 */
public class CommonVariables {

	// /Setup up for commonly used variables that can be accessed when the class
	// is called
	private volatile static CommonVariables instance;

	public Context context;
	public int currentSoundPosition, tapSound, saveSound, numberOfPieces,
			currPieceOnTouch, currSlotOnTouchUp, inPlace, screenH, screenW,
			difficulty, currentPuzzleImagePosition;
	public boolean chimeLoaded, imageSplit, tapLoaded, movingPiece,
			errorLoading, solved, resumePreviousPuzzle, imageReady,
			playChimeSound = true, drawBorders = true, playTapSound = true,
			playMusic = true;

	// the value is the piece to go into it
	public int[] slotOrder;
	public ArrayList<Integer> imagesShown = new ArrayList<Integer>();
	public Random rand = new Random();
	public Resources res;
	public Bitmap image;
	public PuzzlePiece[] puzzlePieces;
	public PuzzleSlot[] puzzleSlots;
	public TextView mStatusText;
	public Button mSaveButton;
	public ImageButton rightWebLinkButton, leftWebLinkButton;
	public float volume;

	public MySoundPool mySoundPool;
	
	Toast toast = null;

	public static CommonVariables getInstance() {
		if (instance == null)
			synchronized (CommonVariables.class) {
				if (instance == null)
					instance = new CommonVariables();
			}
		return instance;
	}

	public boolean setSlots(String string) {
		switch (difficulty) {
		case 0:
			slotOrder = new int[9];
			break;
		case 1:
			slotOrder = new int[16];
			break;
		case 2:
			slotOrder = new int[25];
			break;
		}
		String[] stringSlots = string.split(",");
		for (int i = 0; i < stringSlots.length; i++) {
			try {
				slotOrder[i] = Integer.parseInt(stringSlots[i]);
			} catch (NumberFormatException nfe) {
				return false;
			}
		}
		return true;
	}

	public void jumblePicture() {
		// reorders the images as jumbled
		for (int i = 0; i < numberOfPieces; i++) {
			int oldslot = i;
			int newslot = slotOrder[i];
			if (oldslot != newslot) {
				PuzzlePiece temp = new PuzzlePiece();
				temp = puzzleSlots[oldslot].puzzlePiece;
				puzzleSlots[oldslot].puzzlePiece = puzzleSlots[newslot].puzzlePiece;
				puzzleSlots[oldslot].puzzlePiece.px = puzzleSlots[oldslot].sx;
				puzzleSlots[oldslot].puzzlePiece.py = puzzleSlots[oldslot].sy;
				puzzleSlots[newslot].puzzlePiece = temp;
				puzzleSlots[newslot].puzzlePiece.px = puzzleSlots[newslot].sx;
				puzzleSlots[newslot].puzzlePiece.py = puzzleSlots[newslot].sy;
				temp = null;
			}
		}
	}

	public int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) height
					/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee a final image with both dimensions larger than or equal
			// to the requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

	public Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
			int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}

	// switch a piece in the puzzle
	public void sendPieceToNewSlot(int a, int z) {
		PuzzlePiece temp = new PuzzlePiece();
		temp = puzzleSlots[currPieceOnTouch].puzzlePiece;
		puzzleSlots[a].puzzlePiece = puzzleSlots[z].puzzlePiece;
		puzzleSlots[a].puzzlePiece.px = puzzleSlots[a].sx;
		puzzleSlots[a].puzzlePiece.py = puzzleSlots[a].sy;
		puzzleSlots[z].puzzlePiece = temp;
		puzzleSlots[z].puzzlePiece.px = puzzleSlots[z].sx;
		puzzleSlots[z].puzzlePiece.py = puzzleSlots[z].sy;
		temp = null;
	}

	public void showToast(String message) {
		// Create and show toast for save photo
		if (toast == null) {
			toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 0);
		}
		if (!toast.getView().isShown()) {
			toast.setText(message);
			toast.show();
		} else {
			toast.cancel();
			toast.setText(message);
			toast.show();
		}
	}

	public void playSetSound() {
		// play sound for setting puzzle pieces
		mySoundPool.playSetSound();
	}
}