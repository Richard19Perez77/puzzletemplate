package puzzle;

import android.view.MotionEvent;

/**
 * 
 * An interface that defines methods that will be used no matter what difficulty
 * the current puzzle is.
 * 
 * @author Rick
 * 
 */
public interface Puzzle {

	public void getNewImageLoadedScaledDivided(Thread thread);

	public boolean onTouchEvent(MotionEvent event);

	public void recylceAll();

	public String getPercentComplete();

	public void initTimer();

	public void stopTimer();

	public void resetTimer();

	public double getSolveTime();

	public int getCurrentImage();

	public void getPrevousImageLoadedScaledDivided(Thread loadingThread);

	public boolean divideBitmap();

	public boolean divideBitmapFromPreviousPuzzle();

	public void pause();
}