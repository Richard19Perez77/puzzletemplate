package puzzle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import state.CommonVariables;
import surface.PuzzlePiece;
import surface.PuzzleSlot;
import tac.toki.data.Data;
import android.graphics.Bitmap;
import android.view.MotionEvent;

/**
 * 
 * A class with Hard implementation is a 4 x 4 grid.
 * 
 * @author Rick
 * 
 */
public class HardPuzzleImpl implements Puzzle {

	public int PIECES = 16;
	int bitmapWd4;
	int bitmapHd4;

	public int piecesComplete;
	int jumbledNumber;
	int index;
	boolean isRandom, newImageComplete;
	Date startPuzzle = new Date();
	Date stopPuzzle = new Date();
	long currPuzzleTime = 0;
	CommonVariables cv = CommonVariables.getInstance();

	public HardPuzzleImpl() {
	}

	public void getNewImageLoadedScaledDivided(Thread thread) {
		newImageComplete = false;
		if (thread != null && thread.isAlive())
			thread.interrupt();

		thread = new Thread() {
			@Override
			public void run() {

				while (!newImageComplete) {
					// fill with all valid numbers
					if (cv.imagesShown.isEmpty())
						for (int i = 0; i < Data.PICS.length; i++)
							cv.imagesShown.add(i);

					// get new index value from remaining images
					index = cv.rand.nextInt(cv.imagesShown.size());
					// get the value at that index for new image
					cv.currentPuzzleImagePosition = cv.imagesShown.get(index);
					// remove from list to prevent duplicates
					cv.imagesShown.remove(index);

					cv.image = cv.decodeSampledBitmapFromResource(cv.res,
							Data.PICS[cv.currentPuzzleImagePosition],
							cv.screenW, cv.screenH);
					cv.image = Bitmap.createScaledBitmap(cv.image, cv.screenW,
							cv.screenH, true);

					newImageComplete = divideBitmap();

					if (newImageComplete) {
						resetTimer();
						cv.errorLoading = false;
						cv.solved = false;
						cv.imageReady = true;
						cv.mySoundPool.playChimeSound();
						System.gc();
					} else {
						cv.errorLoading = true;
					}
				}
			}
		};
		thread.start();
	}

	public boolean divideBitmap() {
		cv.numberOfPieces = PIECES;

		cv.puzzlePieces = new PuzzlePiece[PIECES];
		for (int i = 0; i < cv.numberOfPieces; i++)
			cv.puzzlePieces[i] = new PuzzlePiece();

		cv.puzzleSlots = new PuzzleSlot[PIECES];
		for (int i = 0; i < cv.numberOfPieces; i++)
			cv.puzzleSlots[i] = new PuzzleSlot();

		cv.slotOrder = new int[PIECES];

		// set natural order
		for (int i = 0; i < PIECES; i++)
			cv.slotOrder[i] = i;

		isRandom = false;
		while (!isRandom) {
			List<Integer> list = new ArrayList<Integer>();
			for (int i : cv.slotOrder) {
				list.add(i);
			}

			Collections.shuffle(list);
			for (int i = 0; i < list.size(); i++) {
				cv.slotOrder[i] = list.get(i);
			}

			for (int i = 0; i < cv.slotOrder.length; i++) {
				if (cv.slotOrder[i] != i)
					isRandom = true;
			}

			if (cv.slotOrder[0] == 0 && cv.slotOrder[1] == 1
					&& cv.slotOrder[2] == 2 && cv.slotOrder[3] == 3
					&& cv.slotOrder[4] == 4 && cv.slotOrder[5] == 5
					&& cv.slotOrder[6] == 6 && cv.slotOrder[7] == 7
					&& cv.slotOrder[8] == 8 && cv.slotOrder[9] == 9
					&& cv.slotOrder[10] == 10 && cv.slotOrder[11] == 11
					&& cv.slotOrder[12] == 12 && cv.slotOrder[13] == 13
					&& cv.slotOrder[14] == 14 && cv.slotOrder[15] == 15) {
				isRandom = false;
			}
		}

		// re do if the image didn't split correctly
		cv.imageSplit = false;
		piecesComplete = 0;

		while (!cv.imageSplit) {
			int w = cv.image.getWidth();
			int h = cv.image.getHeight();
			bitmapWd4 = w / 4;
			bitmapHd4 = h / 4;
			int x, y;
			for (int i = 0; i < PIECES; i++) {
				if (i < 4) {
					y = 0;
				} else if (i < 8) {
					y = bitmapHd4;
				} else if (i < 12) {
					y = bitmapHd4 * 2;
				} else {
					y = bitmapHd4 * 3;
				}

				x = (i % 4) * bitmapWd4;

				if (cv.puzzlePieces[i].bitmap != null)
					cv.puzzlePieces[i].bitmap.recycle();

				cv.puzzlePieces[i].bitmap = null;
				cv.puzzlePieces[i].bitmap = Bitmap.createBitmap(cv.image, x, y,
						bitmapWd4, bitmapHd4);

				cv.puzzlePieces[i].px = x;
				cv.puzzlePieces[i].px2 = x + bitmapWd4;

				cv.puzzlePieces[i].py = y;
				cv.puzzlePieces[i].py2 = y + bitmapHd4;

				cv.puzzleSlots[i].sx = x;
				cv.puzzleSlots[i].sx2 = x + bitmapWd4;

				cv.puzzleSlots[i].sy = y;
				cv.puzzleSlots[i].sy2 = y + bitmapHd4;

				cv.puzzleSlots[i].puzzlePiece = cv.puzzlePieces[i];
				cv.puzzleSlots[i].slotNum = cv.puzzleSlots[i].puzzlePiece.pieceNum = i;

				piecesComplete++;

			}
			cv.imageSplit = true;
			cv.image.recycle();
			cv.image = null;
		}

		cv.jumblePicture();

		if (piecesComplete == PIECES)
			return true;

		return false;
	}

	@Override
	public boolean divideBitmapFromPreviousPuzzle() {
		cv.numberOfPieces = PIECES;

		cv.puzzlePieces = new PuzzlePiece[PIECES];
		for (int i = 0; i < cv.numberOfPieces; i++)
			cv.puzzlePieces[i] = new PuzzlePiece();

		cv.puzzleSlots = new PuzzleSlot[PIECES];
		for (int i = 0; i < cv.numberOfPieces; i++)
			cv.puzzleSlots[i] = new PuzzleSlot();

		// re do if the image didn't split correctly
		cv.imageSplit = false;
		piecesComplete = 0;

		while (!cv.imageSplit) {
			int w = cv.image.getWidth();
			int h = cv.image.getHeight();
			bitmapWd4 = w / 4;
			bitmapHd4 = h / 4;
			int x, y;
			for (int i = 0; i < PIECES; i++) {
				if (i < 4) {
					y = 0;
				} else if (i < 8) {
					y = bitmapHd4;
				} else if (i < 12) {
					y = bitmapHd4 * 2;
				} else {
					y = bitmapHd4 * 3;
				}

				x = (i % 4) * bitmapWd4;

				if (cv.puzzlePieces[i].bitmap != null)
					cv.puzzlePieces[i].bitmap.recycle();

				cv.puzzlePieces[i].bitmap = null;
				cv.puzzlePieces[i].bitmap = Bitmap.createBitmap(cv.image, x, y,
						bitmapWd4, bitmapHd4);

				cv.puzzlePieces[i].px = x;
				cv.puzzlePieces[i].px2 = x + bitmapWd4;

				cv.puzzlePieces[i].py = y;
				cv.puzzlePieces[i].py2 = y + bitmapHd4;

				cv.puzzleSlots[i].sx = x;
				cv.puzzleSlots[i].sx2 = x + bitmapWd4;

				cv.puzzleSlots[i].sy = y;
				cv.puzzleSlots[i].sy2 = y + bitmapHd4;

				// cv.puzzleSlots[i].puzzlePiece = cv.puzzlePieces[i];
				cv.puzzleSlots[i].slotNum = cv.puzzleSlots[i].puzzlePiece.pieceNum = i;

				piecesComplete++;

			}
			cv.imageSplit = true;
			cv.image.recycle();
			cv.image = null;
		}

		// set slot by creating separate puzzle pieces and reassign individually
		boolean correctlyReassembled = false;
		while (!correctlyReassembled) {
			// use saved slot list to sort
			for (int toSlot = 0; toSlot < cv.slotOrder.length; toSlot++) {

				// get new slot to take piece from and place into correct slot
				int fromSlot = cv.slotOrder[toSlot];
				PuzzlePiece pieceA = cv.puzzlePieces[fromSlot];

				cv.puzzleSlots[toSlot].puzzlePiece = pieceA;
				cv.puzzleSlots[toSlot].puzzlePiece.px = cv.puzzleSlots[toSlot].sx;
				cv.puzzleSlots[toSlot].puzzlePiece.py = cv.puzzleSlots[toSlot].sy;
				cv.puzzleSlots[toSlot].puzzlePiece.px2 = cv.puzzleSlots[toSlot].sx2;
				cv.puzzleSlots[toSlot].puzzlePiece.py2 = cv.puzzleSlots[toSlot].sy2;
				cv.puzzleSlots[toSlot].puzzlePiece.pieceNum = fromSlot;
			}

			correctlyReassembled = true;
			for (int j = 0; j < cv.puzzleSlots.length; j++) {
				int a = cv.puzzleSlots[j].puzzlePiece.pieceNum;
				int b = cv.slotOrder[j];
				if (a != b) {
					correctlyReassembled = false;
				}
			}
		}

		if (piecesComplete == PIECES)
			return true;

		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int newx = (int) event.getX();
		int newy = (int) event.getY();

		// find the piece that was pressed down onto
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			cv.movingPiece = false;
			if (newx < bitmapWd4) {
				// check first row
				if (newy < bitmapHd4) {
					// piece1 pressed on
					cv.currPieceOnTouch = 0;
				} else if (newy < bitmapHd4 * 2) {
					// piece4 pressed on
					cv.currPieceOnTouch = 4;
				} else if (newy < bitmapHd4 * 3) {
					// piece8 pressed on
					cv.currPieceOnTouch = 8;
				} else {
					// piece12 pressed on
					cv.currPieceOnTouch = 12;
				}
			} else if (newx < bitmapWd4 * 2) {
				// check second row
				if (newy < bitmapHd4) {
					// piece1 pressed on
					cv.currPieceOnTouch = 1;
				} else if (newy < bitmapHd4 * 2) {
					// piece5 pressed on
					cv.currPieceOnTouch = 5;
				} else if (newy < bitmapHd4 * 3) {
					// piece9 pressed on
					cv.currPieceOnTouch = 9;
				} else {
					// piece13 pressed on
					cv.currPieceOnTouch = 13;
				}
			} else if (newx < bitmapWd4 * 3) {
				// check third row
				if (newy < bitmapHd4) {
					// piece2 pressed on
					cv.currPieceOnTouch = 2;
				} else if (newy < bitmapHd4 * 2) {
					// piece6 pressed on
					cv.currPieceOnTouch = 6;
				} else if (newy < bitmapHd4 * 3) {
					// piece10 pressed on
					cv.currPieceOnTouch = 10;
				} else {
					// piece14 pressed on
					cv.currPieceOnTouch = 14;
				}
			} else if (newx < bitmapWd4 * 4) {
				// check fourth row
				if (newy < bitmapHd4) {
					// piece3 pressed on
					cv.currPieceOnTouch = 3;
				} else if (newy < bitmapHd4 * 2) {
					// piece7 pressed on
					cv.currPieceOnTouch = 7;
				} else if (newy < bitmapHd4 * 3) {
					// piece11 pressed on
					cv.currPieceOnTouch = 11;
				} else {
					// piece15 pressed on
					cv.currPieceOnTouch = 15;
				}
			}
		}

		if (event.getAction() == MotionEvent.ACTION_UP) {
			cv.movingPiece = false;
			if (newx < bitmapWd4) {
				// check first row
				if (newy < bitmapHd4) {
					// piece1 pressed on
					cv.currSlotOnTouchUp = 0;
				} else if (newy < bitmapHd4 * 2) {
					// piece4 pressed on
					cv.currSlotOnTouchUp = 4;
				} else if (newy < bitmapHd4 * 3) {
					// piece8 pressed on
					cv.currSlotOnTouchUp = 8;
				} else {
					// piece12 pressed on
					cv.currSlotOnTouchUp = 12;
				}
			} else if (newx < bitmapWd4 * 2) {
				// check second row
				if (newy < bitmapHd4) {
					// piece1 pressed on
					cv.currSlotOnTouchUp = 1;
				} else if (newy < bitmapHd4 * 2) {
					// piece5 pressed on
					cv.currSlotOnTouchUp = 5;
				} else if (newy < bitmapHd4 * 3) {
					// piece9 pressed on
					cv.currSlotOnTouchUp = 9;
				} else {
					// piece13 pressed on
					cv.currSlotOnTouchUp = 13;
				}
			} else if (newx < bitmapWd4 * 3) {
				// check third row
				if (newy < bitmapHd4) {
					// piece2 pressed on
					cv.currSlotOnTouchUp = 2;
				} else if (newy < bitmapHd4 * 2) {
					// piece6 pressed on
					cv.currSlotOnTouchUp = 6;
				} else if (newy < bitmapHd4 * 3) {
					// piece10 pressed on
					cv.currSlotOnTouchUp = 10;
				} else {
					// piece14 pressed on
					cv.currSlotOnTouchUp = 14;
				}
			} else if (newx < bitmapWd4 * 4) {
				// check fourth row
				if (newy < bitmapHd4) {
					// piece3 pressed on
					cv.currSlotOnTouchUp = 3;
				} else if (newy < bitmapHd4 * 2) {
					// piece7 pressed on
					cv.currSlotOnTouchUp = 7;
				} else if (newy < bitmapHd4 * 3) {
					// piece11 pressed on
					cv.currSlotOnTouchUp = 11;
				} else {
					// piece15 pressed on
					cv.currSlotOnTouchUp = 15;
				}
			}

			// check for image to be in new slot
			if (cv.currPieceOnTouch != cv.currSlotOnTouchUp) {
				cv.sendPieceToNewSlot(cv.currPieceOnTouch, cv.currSlotOnTouchUp);
				cv.playSetSound();
			} else {
				cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.px = cv.puzzleSlots[cv.currSlotOnTouchUp].sx;
				cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.py = cv.puzzleSlots[cv.currSlotOnTouchUp].sy;
			}
		}

		if (event.getAction() == MotionEvent.ACTION_MOVE) {
			cv.movingPiece = true;
			if (cv.currPieceOnTouch >= 0 && cv.currPieceOnTouch < PIECES) {
				cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.px = newx
						- bitmapWd4 / 2;
				cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.py = newy
						- bitmapHd4 / 2;
			}
		}

		cv.inPlace = 0;
		for (int i = 0; i < cv.numberOfPieces; i++) {
			if (cv.puzzleSlots[i].slotNum == cv.puzzleSlots[i].puzzlePiece.pieceNum) {
				cv.inPlace++;
			}
		}

		if (cv.inPlace == cv.numberOfPieces) {
			stopTimer();
			cv.solved = true;
			cv.showToast("Solve Time: " + getSolveTime());
			return false;
		}

		return true;
	}

	@Override
	public void recylceAll() {
		if (cv.image != null)
			cv.image.recycle();

		for (int i = 0; i < cv.puzzlePieces.length; i++)
			if (cv.puzzlePieces != null)
				if (cv.puzzlePieces[i] != null)
					if (cv.puzzlePieces[i].bitmap != null)
						cv.puzzlePieces[i].bitmap.recycle();

	}

	@Override
	public String getPercentComplete() {
		return "" + (float) piecesComplete / PIECES;
	}

	@Override
	public void initTimer() {
		startPuzzle = new Date();
	}

	@Override
	public void stopTimer() {
		stopPuzzle = new Date();
		currPuzzleTime += stopPuzzle.getTime() - startPuzzle.getTime();
	}

	@Override
	public void resetTimer() {
		currPuzzleTime = 0;
		startPuzzle = new Date();
	}

	@Override
	public double getSolveTime() {
		return currPuzzleTime / 1000.0;
	}

	@Override
	public int getCurrentImage() {
		// return current image
		return index;
	}

	@Override
	public void getPrevousImageLoadedScaledDivided(Thread thread) {
		// gets an image based on difficulty, image number and order from save
		// state
		newImageComplete = false;
		if (thread != null && thread.isAlive())
			thread.interrupt();

		thread = new Thread() {
			@Override
			public void run() {
				while (!newImageComplete) {
					// get new index value and then remove index
					index = cv.currentPuzzleImagePosition;

					cv.image = cv.decodeSampledBitmapFromResource(cv.res,
							Data.PICS[cv.currentPuzzleImagePosition],
							cv.screenW, cv.screenH);

					cv.image = Bitmap.createScaledBitmap(cv.image, cv.screenW,
							cv.screenH, true);

					newImageComplete = divideBitmapFromPreviousPuzzle();

					if (newImageComplete) {
						resetTimer();
						cv.errorLoading = false;
						cv.solved = false;
						cv.imageReady = true;
						cv.mySoundPool.playChimeSound();
						System.gc();
					} else {
						cv.errorLoading = true;
					}
				}
			}
		};
		thread.start();
	}

	@Override
	public void pause() {
		// stop timer for the current puzzle
		stopTimer();
	}
}