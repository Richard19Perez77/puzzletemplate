package surface;

import photosave.SavePhoto;
import puzzle.Puzzle;
import puzzle.PuzzleFactory;
import sound.MyMediaPlayer;
import state.CommonVariables;
import tac.toki.data.Data;
import tac.toki.puzzleimage.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A class to hold the surface of the puzzle and the thread for updating physics
 * and drawing.
 *
 * Currently no splash screen implemented.
 * 
 * @author Rick
 * 
 */
public class PuzzleSurface extends SurfaceView implements
		SurfaceHolder.Callback {

	public static final int EASY = 0;
	public static final int HARD = 1;
	public static final int VERY_HARD = 2;

	public static final int TRANS_VALUE = (255 / 2);
	public static final int STROKE_VALUE = 5;

	Thread loadingThread;

	Intent intent;
	boolean mExternalStorageAvailable;
	boolean mExternalStorageWriteable;
	Toast toast;

	public Paint borderPaintA;
	public Paint borderPaintB;
	public Paint transPaint;
	public Paint fullPaint;

	PuzzleFactory pf = new PuzzleFactory();
	Puzzle puzzle;
	public PuzzleThread puzzleThread;

	boolean difficultyChanged;

	public MyMediaPlayer myMediaPlayer;
	Handler myHandler;

	private boolean recreateThread;
	CommonVariables cv = CommonVariables.getInstance();
	public boolean introFinished;
	public TextView messageTextView;

	class IncomingHandlerCallback implements Handler.Callback {
		@Override
		public boolean handleMessage(Message m) {
			// handle message code
			cv.mStatusText.setVisibility(m.getData().getInt("show"));
			cv.mStatusText.setText(m.getData().getString("message"));
			return true;
		}
	}

	public PuzzleSurface(Context context, AttributeSet attrs) {
		super(context, attrs);
		// set context for access in other classes
		cv.context = context;
		cv.res = context.getResources();

		// register our interest in hearing about changes to our surface
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);

		borderPaintA = new Paint();
		borderPaintA.setStyle(Paint.Style.STROKE);
		borderPaintA.setStrokeWidth(STROKE_VALUE);
		borderPaintA.setColor(Color.LTGRAY);
		borderPaintA.setAlpha(TRANS_VALUE);

		borderPaintB = new Paint();
		borderPaintB.setStyle(Paint.Style.STROKE);
		borderPaintB.setStrokeWidth(STROKE_VALUE);
		borderPaintB.setColor(Color.DKGRAY);
		borderPaintB.setAlpha(TRANS_VALUE);

		transPaint = new Paint();
		transPaint.setAlpha(TRANS_VALUE);
		transPaint.setStyle(Paint.Style.FILL);

		fullPaint = new Paint();
		myHandler = new Handler(new IncomingHandlerCallback());

		// create thread only; it's started in surfaceCreated()
		puzzleThread = new PuzzleThread(holder, context, myHandler);
	}

	public void nextImage() {
		synchronized (puzzleThread.mSurfaceHolder) {
			if (cv.mSaveButton.isShown())
				cv.mSaveButton.setVisibility(INVISIBLE);

			AlertDialog.Builder builder = new AlertDialog.Builder(cv.context);

			builder.setTitle("Radical\u2605Appwards\nSolve Time = "
					+ puzzle.getSolveTime() + " secs.");
			builder.setMessage("Do you want to Save this image?");
			builder.setPositiveButton("Yup",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							new SavePhoto(cv.currentPuzzleImagePosition);
							// actaullySavePhoto(cv.currentImage);
							hideButtons();
							puzzleThread.setState(PuzzleThread.STATE_PAUSE);
							cv.imageReady = false;
							puzzle.getNewImageLoadedScaledDivided(loadingThread);
							dialog.dismiss();
						}
					});

			builder.setNegativeButton("Nope",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							hideButtons();
							puzzleThread.setState(PuzzleThread.STATE_PAUSE);
							cv.imageReady = false;
							puzzle.getNewImageLoadedScaledDivided(loadingThread);
							dialog.dismiss();
						}
					});

			builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					// if the dialog is canceled give the option to start it
					// again
					if (!cv.mSaveButton.isShown())
						cv.mSaveButton.setVisibility(VISIBLE);
					if (cv.mStatusText.isShown())
						cv.mStatusText.setVisibility(INVISIBLE);
					puzzleThread.setState(PuzzleThread.STATE_RUNNING);
					dialog.dismiss();
				}
			});

			AlertDialog alert = builder.create();
			alert.show();
		}
	}

	public void showButtons() {
		if (cv.mSaveButton.getVisibility() == View.INVISIBLE)
			cv.mSaveButton.setVisibility(VISIBLE);
		if (cv.rightWebLinkButton.getVisibility() == View.INVISIBLE)
			cv.rightWebLinkButton.setVisibility(VISIBLE);
		if (cv.leftWebLinkButton.getVisibility() == View.INVISIBLE)
			cv.leftWebLinkButton.setVisibility(VISIBLE);
	}

	public void hideButtons() {
		if (cv.mSaveButton.getVisibility() == View.VISIBLE)
			cv.mSaveButton.setVisibility(INVISIBLE);
		if (cv.rightWebLinkButton.getVisibility() == View.VISIBLE)
			cv.rightWebLinkButton.setVisibility(INVISIBLE);
		if (cv.leftWebLinkButton.getVisibility() == View.VISIBLE)
			cv.leftWebLinkButton.setVisibility(INVISIBLE);
	}

	public void showToast(Context cont, String message) {
		// create if not, or set text to it
		if (toast == null) {
			toast = Toast.makeText(cont, message, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 0);
		}
		if (!toast.getView().isShown()) {
			toast.setText(message);
			toast.show();
		} else {
			toast.setText(message);
		}
	}

	@Override
	public void onWindowFocusChanged(boolean hasWindowFocus) {
		if (!hasWindowFocus) {
			puzzleThread.pause();
		}
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		puzzleThread.surfaceChanged(width, height);
	}

	public void surfaceCreated(SurfaceHolder holder) {
		// create a switch for the states
		switch (puzzleThread.getState()) {
		case BLOCKED:
			// The thread is waiting.
			break;
		case NEW:
			// The thread may be run.
			puzzleThread.start();
			puzzleThread.setRunning(true);
			break;
		case RUNNABLE:
			// The thread is blocked and waiting for a lock.
			break;
		case TERMINATED:
			puzzleThread = new PuzzleThread(holder, cv.context, myHandler);
			puzzleThread.start();
			puzzleThread.setRunning(true);
			break;
		case TIMED_WAITING:
			// The thread has been terminated.
			break;
		case WAITING:
			// The thread is waiting for a specified amount of time.
			break;
		default:
			break;
		}
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
		puzzleThread.setRunning(false);
		while (retry) {
			try {
				puzzleThread.join();
				retry = false;
			} catch (InterruptedException e) {
			}
		}
	}

	@Override
	public boolean performClick() {
		super.performClick();
		return true;
	}

	/**
	 * Perform Click is called on UP press to perform accessibility type
	 * actions.
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		synchronized (puzzleThread.getSurfaceHolder()) {
			if (event.getAction() == MotionEvent.ACTION_UP) {
				performClick();
			}
			if (recreateThread) {
				recreateThread = false;
				if (puzzleThread.getState() == Thread.State.TERMINATED) {
					puzzleThread = new PuzzleThread(getHolder(), cv.context,
							myHandler);
					puzzleThread.start();
					puzzleThread.setRunning(true);
				} else if (puzzleThread.getState() == Thread.State.NEW) {
					puzzleThread.start();
					puzzleThread.setRunning(true);
				}
			}
			if (introFinished)
				switch (puzzleThread.mMode) {
				case PuzzleThread.STATE_PAUSE:
					puzzleThread.setState(PuzzleThread.STATE_RUNNING);
					return false;
				case PuzzleThread.STATE_RUNNING:
					if (cv.solved) {
						// win screen is before the next image button is pressed
						if (cv.mSaveButton.getVisibility() == View.VISIBLE)
							cv.mSaveButton.setVisibility(INVISIBLE);
						else
							cv.mSaveButton.setVisibility(VISIBLE);

						if (cv.rightWebLinkButton.getVisibility() == View.VISIBLE)
							cv.rightWebLinkButton.setVisibility(INVISIBLE);
						else
							cv.rightWebLinkButton.setVisibility(VISIBLE);

						if (cv.leftWebLinkButton.getVisibility() == View.VISIBLE)
							cv.leftWebLinkButton.setVisibility(INVISIBLE);
						else
							cv.leftWebLinkButton.setVisibility(VISIBLE);

						return false;
					} else if (cv.imageReady) {
						return puzzle.onTouchEvent(event);
					}
					break;
				}
			return super.onTouchEvent(event);
		}
	}

	public void recreatePuzzle() {
		// to recreate hide the UI
		puzzleThread.setState(PuzzleThread.STATE_PAUSE);
		cv.imageReady = false;
		puzzle = pf.getPuzzle(cv.difficulty);
		puzzle.getNewImageLoadedScaledDivided(loadingThread);
		hideButtons();
	}

	public void solve() {
		for (int i = 0; i < cv.numberOfPieces; i++) {
			int oldslot = 8 - i;
			int newslot = cv.slotOrder[i];
			if (oldslot != newslot) {
				PuzzlePiece temp = new PuzzlePiece();
				temp = cv.puzzleSlots[oldslot].puzzlePiece;
				cv.puzzleSlots[oldslot].puzzlePiece = cv.puzzleSlots[newslot].puzzlePiece;
				cv.puzzleSlots[oldslot].puzzlePiece.px = cv.puzzleSlots[oldslot].sx;
				cv.puzzleSlots[oldslot].puzzlePiece.py = cv.puzzleSlots[oldslot].sy;
				cv.puzzleSlots[newslot].puzzlePiece = temp;
				cv.puzzleSlots[newslot].puzzlePiece.px = cv.puzzleSlots[newslot].sx;
				cv.puzzleSlots[newslot].puzzlePiece.py = cv.puzzleSlots[newslot].sy;
				temp = null;
			}
		}
		cv.solved = true;
	}

	public void devArtActivity() {
		Intent intent2 = new Intent(Intent.ACTION_VIEW);
		intent2.setData(Uri.parse(Data.DEVART_LINK));
		cv.context.startActivity(intent2);
	}

	public void facebookActivity() {
		Intent intent1 = new Intent(Intent.ACTION_VIEW);
		intent1.setData(Uri.parse(Data.FACEBOOK_LINK));
		cv.context.startActivity(intent1);
	}

	public void cleanUp() {
		if (puzzle != null)
			puzzle.recylceAll();
		if (loadingThread != null && loadingThread.isAlive()) {
			loadingThread.interrupt();
			loadingThread = null;
		}
	}

	public void startTimer() {
		// start timer for new puzzle in puzzle
		if (puzzle != null)
			puzzle.initTimer();
	}

	public String getSlotString() {
		String s = "";
		for (int i = 0; i < cv.puzzleSlots.length; i++) {
			if (i == 0) {
				s = "" + cv.puzzleSlots[i].puzzlePiece.pieceNum;
			} else {
				s = s + "," + cv.puzzleSlots[i].puzzlePiece.pieceNum;
			}
		}
		return s;
	}

	public void jumblePictureFromOldPuzzle() {
		for (int i = 0; i < cv.numberOfPieces; i++) {
			int oldslot = i;
			int newslot = cv.slotOrder[i];
			if (oldslot != newslot) {
				PuzzlePiece temp = new PuzzlePiece();
				temp = cv.puzzleSlots[oldslot].puzzlePiece;
				cv.puzzleSlots[oldslot].puzzlePiece = cv.puzzleSlots[newslot].puzzlePiece;
				cv.puzzleSlots[oldslot].puzzlePiece.px = cv.puzzleSlots[oldslot].sx;
				cv.puzzleSlots[oldslot].puzzlePiece.py = cv.puzzleSlots[oldslot].sy;
				cv.puzzleSlots[newslot].puzzlePiece = temp;
				cv.puzzleSlots[newslot].puzzlePiece.px = cv.puzzleSlots[newslot].sx;
				cv.puzzleSlots[newslot].puzzlePiece.py = cv.puzzleSlots[newslot].sy;
				temp = null;
			}
		}
	}

	public void onResume() {
		if (puzzleThread.getState() == Thread.State.TERMINATED) {
			recreateThread = true;
		}
	}

	public void onPause() {
		if (puzzleThread != null) {
			puzzleThread.pause();
			puzzleThread.setRunning(false);
		}
	}

	public class PuzzleThread extends Thread {

		public SurfaceHolder mSurfaceHolder;
		public boolean mRun = false;
		public int mMode;
		public final static int STATE_PAUSE = 0;
		public final static int STATE_RUNNING = 1;
		public Handler mHandler;

		public PuzzleThread(SurfaceHolder surfaceHolder, Context context,
				Handler handler) {
			mSurfaceHolder = surfaceHolder;
			mHandler = handler;
			cv.context = context;
		}

		@Override
		public void run() {
			while (mRun) {
				Canvas c = null;
				try {
					c = mSurfaceHolder.lockCanvas(null);
					if (c != null) {
						synchronized (mSurfaceHolder) {
							if (mMode == STATE_RUNNING)
								updatePhysics();

							doDraw(c);
						}
					}
				} finally {
					if (c != null) {
						mSurfaceHolder.unlockCanvasAndPost(c);
					}
				}
			}
		}

		public void setRunning(boolean b) {
			synchronized (mSurfaceHolder) {
				mRun = b;
			}
		}

		public SurfaceHolder getSurfaceHolder() {
			return mSurfaceHolder;
		}

		public void updatePhysics() {
			if (difficultyChanged) {
				difficultyChanged = false;
				recreatePuzzle();
			}
		}

		public void doDraw(Canvas canvas) {
			if (canvas != null) {
				if (puzzle == null) {
					if (cv.resumePreviousPuzzle) {
						puzzle = pf.getPuzzle(cv.difficulty);
						cv.imageReady = false;
						puzzle.getPrevousImageLoadedScaledDivided(loadingThread);
						puzzle.initTimer();
					} else if (difficultyChanged) {
						difficultyChanged = false;
						recreatePuzzle();
					} else {
						// start with an easy puzzle
						puzzle = pf.getPuzzle(EASY);
						cv.imageReady = false;
						puzzle.getNewImageLoadedScaledDivided(loadingThread);
						puzzle.initTimer();
					}
				} else if (cv.imageReady) {

					canvas.drawColor(Color.BLACK);

					// draw a moving piece away from its original location
					if (cv.movingPiece) {
						for (int i = 0; i < cv.numberOfPieces; i++) {
							// draw pieces
							if (!cv.puzzleSlots[i].puzzlePiece.bitmap
									.isRecycled() && cv.currPieceOnTouch != i)
								canvas.drawBitmap(
										cv.puzzleSlots[i].puzzlePiece.bitmap,
										cv.puzzleSlots[i].puzzlePiece.px,
										cv.puzzleSlots[i].puzzlePiece.py, null);
							// draw border to pieces
							if (!cv.solved && cv.drawBorders)
								canvas.drawRect(
										cv.puzzleSlots[i].sx,
										cv.puzzleSlots[i].sy,
										cv.puzzleSlots[i].sx
												+ cv.puzzleSlots[i].puzzlePiece.bitmap
														.getWidth(),
										cv.puzzleSlots[i].sy
												+ cv.puzzleSlots[i].puzzlePiece.bitmap
														.getHeight(),
										borderPaintA);
						}

						// draw the actual piece
						if (!cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap
								.isRecycled()) {

							// draw moving image in original location
							canvas.drawBitmap(
									cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap,
									cv.puzzleSlots[cv.currPieceOnTouch].sx,
									cv.puzzleSlots[cv.currPieceOnTouch].sy,
									transPaint);

							// draw border around original piece location
							canvas.drawRect(
									cv.puzzleSlots[cv.currPieceOnTouch].sx,
									cv.puzzleSlots[cv.currPieceOnTouch].sy,
									cv.puzzleSlots[cv.currPieceOnTouch].sx
											+ cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap
													.getWidth(),
									cv.puzzleSlots[cv.currPieceOnTouch].sy
											+ cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap
													.getHeight(), borderPaintB);

							// draw moving piece
							canvas.drawBitmap(
									cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap,
									cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.px,
									cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.py,
									fullPaint);

							// draw border around moving piece
							if (cv.drawBorders)
								canvas.drawRect(
										cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.px
												+ (STROKE_VALUE / 2),
										cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.py
												+ (STROKE_VALUE / 2),
										cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.px
												+ cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap
														.getWidth()
												- (STROKE_VALUE / 2),
										cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.py
												+ cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap
														.getHeight()
												- (STROKE_VALUE / 2),
										borderPaintA);
						}
					} else {
						for (int i = 0; i < cv.numberOfPieces; i++) {
							if (!cv.puzzleSlots[i].puzzlePiece.bitmap
									.isRecycled()) {
								// draw pieces
								canvas.drawBitmap(
										cv.puzzleSlots[i].puzzlePiece.bitmap,
										cv.puzzleSlots[i].puzzlePiece.px,
										cv.puzzleSlots[i].puzzlePiece.py, null);
								// draw borders
								if (!cv.solved && cv.drawBorders)
									canvas.drawRect(
											cv.puzzleSlots[i].sx,
											cv.puzzleSlots[i].sy,
											cv.puzzleSlots[i].sx
													+ cv.puzzleSlots[i].puzzlePiece.bitmap
															.getWidth(),
											cv.puzzleSlots[i].sy
													+ cv.puzzleSlots[i].puzzlePiece.bitmap
															.getHeight(),
											borderPaintA);
							}
						}
					}
				} else {
					// the image is loading
					if (cv.errorLoading) {
						canvas.drawColor(Color.RED);
					} else {
						// draw different colors for
						canvas.drawColor(Color.BLACK);
					}
				}
			}
		}

		public void toggleSetSound() {
			synchronized (mSurfaceHolder) {
				if (cv.playTapSound) {
					cv.playTapSound = false;
					cv.showToast("Set Effect Off");
				} else {
					cv.playTapSound = true;
					cv.showToast("Set Effect On");
				}
			}
		}

		public void toggleBorder() {
			synchronized (mSurfaceHolder) {
				if (cv.drawBorders) {
					cv.drawBorders = false;
					cv.showToast("Borders Off");
				} else {
					cv.drawBorders = true;
					cv.showToast("Borders On");
				}
			}
		}

		public void toggleWinSound() {
			synchronized (mSurfaceHolder) {
				if (cv.playChimeSound) {
					cv.playChimeSound = false;
					cv.showToast("Win Effect Off");
				} else {
					cv.playChimeSound = true;
					cv.showToast("Win Effect On");
				}
			}
		}

		public void toggleMusic() {
			synchronized (mSurfaceHolder) {
				if (cv.playMusic) {
					cv.playMusic = false;
					myMediaPlayer.pause();
					cv.showToast("Music Off");
				} else {
					cv.playMusic = true;
					myMediaPlayer.resume();
					cv.showToast("Music On");
				}
			}
		}

		public void setState(int mode) {
			synchronized (mSurfaceHolder) {
				setState(mode, null);
			}
		}

		public void setState(int mode, CharSequence message) {
			synchronized (mSurfaceHolder) {
				mMode = mode;
				Message msg;
				Bundle bundle;
				switch (mMode) {
				case STATE_RUNNING:
					msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					b.putString("message", "");
					b.putInt("show", View.INVISIBLE);
					msg.setData(b);
					mHandler.sendMessage(msg);
					break;
				case STATE_PAUSE:
					Resources res = cv.context.getResources();
					CharSequence str = "";
					str = res.getText(R.string.message_text);
					if (message != null) {
						str = message + "\n" + str;
					}
					msg = mHandler.obtainMessage();
					bundle = new Bundle();
					bundle.putString("message", str.toString());
					bundle.putInt("show", View.VISIBLE);
					msg.setData(bundle);
					mHandler.sendMessage(msg);
					break;
				}
			}
		}

		public void surfaceChanged(int width, int height) {
			// synchronized to make sure these all change atomically
			synchronized (mSurfaceHolder) {
				cv.screenW = width;
				cv.screenH = height;
			}
		}

		public void pause() {
			synchronized (mSurfaceHolder) {
				if (puzzle != null)
					puzzle.pause();
				setState(STATE_PAUSE);
			}
		}

		public void setDifficulty(int diff) {
			synchronized (mSurfaceHolder) {
				if (!cv.imageReady)
					cv.showToast("Please wait for current image to load first.");
				else {
					if (diff != cv.difficulty) {
						// show toast of new difficulty
						if (diff == 0)
							cv.showToast("Easy 3 x 3");
						else if (diff == 1)
							cv.showToast("Hard 4 x 4");
						else if (diff == 2)
							cv.showToast("Very Hard 5 x 5");
						cv.difficulty = diff;
						// difficultyChanged = true;
						recreatePuzzle();
					}
				}
			}
		}

		public Bundle saveState(Bundle map) {
			synchronized (mSurfaceHolder) {
				if (map != null) {
					// map.putInt("difficulty", cv.difficulty);
				}
			}
			return map;
		}
	}
}