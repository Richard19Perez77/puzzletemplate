/**
 * Automatically generated file. DO NOT MODIFY
 */
package tac.toki.puzzleimage;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String PACKAGE_NAME = "tac.toki.puzzleimage";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 23;
  public static final String VERSION_NAME = "";
}
